$(document).ready(function(){
	var lookup = function(query){
		jQuery.get("{% url 'lookup' %}", {'word' : query}, function(response){
			var display = response['word'];
			var error = response['error'];
			if (error != ''){
				$('#display').html(error+'<br/>'+display);
			} else {
				$('#display').html(display);
			}
			$('.word').click(function(){
				lookup($(this).text());
			});
		});
	};
	$('#query').submit(function(){
		lookup($('#word').val());
	});
});
