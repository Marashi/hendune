from django.shortcuts import render
from django.http import HttpResponse
from hendune.models import Word
import traceback
import json
import re
import enchant

keys = None
class WordNotFound(Exception):
	pass
# Create your views here.
def ready_html(html):
	sanitized_html = html.replace('&', '&amp;').replace('\n', '<br/>')
	return sanitized_html
def dictionary(request):
	return render(request, 'hendune/index.html')
def js(request):
	return render(request, 'hendune/hendune.js')
def lookup(request):
	global keys
	try:
		query = request.GET.get('word')
		show_suggestion = ''
		words = Word.objects.filter(key=query.upper()).all()
		if keys is None:
			keys = {i[0]:None for i in Word.objects.values_list('key')}
		definition_html = ''
		if len(words)==0:
			en = enchant.request_dict("en_US")
			suggestions = en.suggest(query)
			if len(suggestions)!=0:
				show_suggestion = 'Did you mean: '
				for word in suggestions:
					if word.upper() in keys:
						show_suggestion += '<a class="word" href="#">'+word+'</a>, '		
			raise WordNotFound
		for word in words:
			definition = word.definition
			definition_keys = {}
			for key in re.findall('([A-Za-z]{3,})', definition):
				if key != query and key.upper() in keys:
					definition_keys[key]= None
			for key in definition_keys:
				definition = re.sub('(\s)('+key+')([\s\.,])', r'\1<a class="word" href="#">\2</a>\3', definition, flags=re.IGNORECASE)
			definition_html = definition_html + r'<div class="definition panel">' + ready_html(definition) + r'</div>'
		return HttpResponse(json.dumps({'word':definition_html, 'error': ''}), content_type='application/json')
	except:
		return HttpResponse(json.dumps({'word': show_suggestion, 'error': ready_html('Couldn\'t find the definition'), 'exception': ready_html(traceback.format_exc())}), content_type='application/json')
