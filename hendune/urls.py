from django.conf.urls import patterns, url

from hendune import views

urlpatterns = patterns('', 
	url(r'^$', views.dictionary , name='dictionary'),
	url(r'^hendune.js$', views.js , name='js'),
	url(r'^lookup$', views.lookup, name='lookup'),
)
