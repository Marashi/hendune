import re
import os
dictionary_file = open(os.path.join(os.path.dirname(__file__), 'pg29765.txt'), 'r')
dictionary_text = dictionary_file.read().replace('\r\n', '\n')[3:]
dictionary_file.close()
# regex = r'\n\n([A-Z][A-Z'\ \d-]*)\n(.*?)(\n\n[A-Z][A-Z'\ \d-]*\n)'
extracted_dictionary = re.split(r"\n\n([A-Z][A-Z'\ \d-]*)\n(.*?)\n\n([A-Z][A-Z'\ \d-]*\n)", dictionary_text, flags=re.DOTALL+re.MULTILINE)
dictionary = {extracted_dictionary[i].strip(): []  for i in range(1, len(extracted_dictionary), 2)}
for i in range(1, len(extracted_dictionary), 2):
	dictionary[extracted_dictionary[i].strip()].append(extracted_dictionary[i+1].strip())

if __name__ == '__main__':
	import os
	while True:
		try:
			word=raw_input('>')
			os.system('clear')
		except:
			break
		try:
			for definition in dictionary[word.upper()]:
				print definition
				print '============================================='
		except:
			print 'word', word, 'did not found'
