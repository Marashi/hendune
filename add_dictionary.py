from django.db import transaction
from hendune.dictionary import dictionary
from hendune.models import Word

@transaction.commit_manually
def add_to_dict():
	for key in dictionary:
		for j in dictionary[key]:
			word = Word(key=key, definition=j)
			word.save()
	transaction.commit()
print 'adding'
add_to_dict()
print 'finished'
